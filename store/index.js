export const state = () => ({
  LAMPORTS_PER_SOL: 1000000000,
  user: null,
  favorites: [],
  follows: [],
  hiddens: [],
  balance: 0,
  course: 0,
})

export const actions = {
  async favorites({ commit }) {
    await this.$axios
      .$get(`/nft/favorites?userId=${this.$cookiz.get('user_id')}`)
      .then((res) => {
        commit('setFavorites', res)
      })
    this.$axios
      .$get(`/collection/favorites?userId=${this.$cookiz.get('user_id')}`)
      .then((res) => {
        commit('setFavoritesCollection', res)
      })
  },
  follows({ commit }) {
    this.$axios
      .$get(`/account/follows?userId=${this.$cookiz.get('user_id')}`)
      .then((res) => {
        commit('setFollowers', res)
      })
      .catch((err) => {
        console.error(err)
      })
  },
  hiddens({ commit }) {
    this.$axios
      .$get(`/account/hidden?userId=${this.$cookiz.get('user_id')}`)
      .then((res) => {
        commit('setHiddens', res)
      })
      .catch((err) => {
        console.error(err)
      })
  },
  async auth({ commit, dispatch }, key) {
    this.$cookiz.set('publicKey', key)

    await this.$axios
      .$get('/account', {
        walletAddress: key,
      })
      .then((res) => {
        commit('setUser', res)
        this.$cookiz.set('user_id', res.id)
      })
      .catch((err) => {
        console.error(err)
      })

    dispatch('favorites')
    dispatch('follows')
    dispatch('hiddens')
  },
  setBalance({ commit }, balance) {
    commit('balance', balance)
  },
  setCourse({ commit }, course) {
    commit('course', course)
  },
}

export const mutations = {
  logout(state) {
    state.user = null
    state.favorites = []
  },
  setUser(state, user) {
    state.user = user
  },
  setFavorites(state, favorites) {
    state.favorites = favorites.items
  },
  setFavoritesCollection(state, favorites) {
    favorites.collections.forEach((element) => {
      state.favorites.push(element)
    })
  },
  addItemToFavorites(state, item) {
    state.favorites.push(item)
  },
  removeItemToFavorites(state, id) {
    const result = state.favorites.findIndex((item) => item.id === id)
    state.favorites.splice(result, 1)
  },
  setFollowers(state, follows) {
    const ids = []
    follows.forEach((element) => {
      ids.push(element.id)
    })
    state.follows = ids
  },
  setHiddens(state, hiddens) {
    const ids = []
    hiddens.forEach((element) => {
      ids.push(element.id)
    })
    state.hiddens = ids
  },
  balance(state, balance) {
    state.balance = balance
  },
  course(state, course) {
    state.course = course
  },
}
