export default function ({ redirect, app }) {
  const key = app.$cookiz.get('publicKey') || null
  if (!key) {
    return redirect('/login')
  }
}
