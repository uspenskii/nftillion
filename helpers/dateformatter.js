function dateFormat(data) {
  let date = new Date(data)
  let month = date.getMonth() + 1
  let minutes = date.getMinutes()
  let day = date.getDate()
  if (month < 10) {
    month = '0' + month
  }
  if (minutes < 10) {
    minutes = '0' + minutes
  }
  if (day < 10) {
    day = '0' + day
  }

  let array = {
    date: day + '.' + month + '.' + date.getFullYear(),
    time: date.getHours() + ':' + minutes,
  }
  return array
}

function dateFormatInput(data) {
  let date = new Date(data)
  let month = date.getMonth() + 1
  let minutes = date.getMinutes()
  let day = date.getDate()
  if (month < 10) {
    month = '0' + month
  }
  if (minutes < 10) {
    minutes = '0' + minutes
  }
  if (day < 10) {
    day = '0' + day
  }

  let array = {
    date: day + '-' + month + '-' + date.getFullYear(),
    time: date.getHours() + ':' + minutes,
  }
  return array
}

export { dateFormat, dateFormatInput }
