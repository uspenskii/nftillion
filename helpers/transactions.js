import { Transaction } from '@solana/web3.js'

export const sendTransactionWithRetryWithKeypair = async (
  connection,
  wallet,
  instructions,
  signers,
  commitment = 'singleGossip',
  includesFeePayer = false,
  block,
  beforeSend
) => {
  console.log('sendTransactionWithRetryWithKeypair')

  const transaction = new Transaction()
  instructions.forEach((instruction) => transaction.add(instruction))
  transaction.recentBlockhash = (
    block || (await connection.getRecentBlockhash(commitment))
  ).blockhash

  if (includesFeePayer) {
    transaction.setSigners(...signers.map((s) => s.publicKey))
  } else {
    transaction.setSigners(wallet.publicKey, ...signers.map((s) => s.publicKey))
  }

  if (signers.length > 0) {
    transaction.sign(...[wallet, ...signers])
  } else {
    transaction.sign(wallet)
  }

  if (beforeSend) {
    beforeSend()
  }

  const { txid, slot } = await sendSignedTransaction({
    connection,
    signedTransaction: transaction,
  })

  return { txid, slot }
}

export async function sendSignedTransaction({
  signedTransaction,
  connection,
  timeout = 15000,
}) {
  console.log('signedTransaction', signedTransaction)
  const rawTransaction = signedTransaction.serialize()
  const slot = 0
  console.log('rawTransaction', rawTransaction)
  const txid = await connection.sendRawTransaction(rawTransaction, {
    skipPreflight: true,
  })

  console.log('Started awaiting confirmation for', txid)
  await connection.confirmTransaction(txid)

  return { txid, slot }
}
