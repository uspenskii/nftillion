import { sha256 } from 'js-sha256'

import encrypter from 'object-encrypter'

const CRYPTO_SECRET =
  '985AAF946C485E92957EE0FA1D998A75CFD6241F18AA43E31606130977DC8A62'

function hasher(publicKey, timestamp = Date.now(), returnHash = true) {
  let usDate = timestamp.toString()
  const shaHash = sha256.hmac(usDate, publicKey)
  let salt = 0
  for (let i = 0; i < shaHash.length; i++) {
    const char = shaHash.charCodeAt(i)
    salt = (salt << 5) - salt + char
    salt = salt & salt
  }
  const hash = shaHash + salt
  if (returnHash) {
    const engine = encrypter(CRYPTO_SECRET)
    return engine.encrypt({ publicKey, timestamp, hash })
  }
  return hash
}

export default hasher
