export default {
  head: {
    title: 'nftillion',
    htmlAttrs: {
      lang: 'en',
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' },
      { name: 'format-detection', content: 'telephone=no' },
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      {
        rel: 'stylesheet',
        href: 'https://fonts.googleapis.com/css2?family=Poppins:wght@400;500;600;700&family=Roboto:wght@500;700;900&display=swap',
      },
    ],
    script: [
      {
        src: 'https://unpkg.com/lightweight-charts/dist/lightweight-charts.standalone.production.js',
        body: true,
      },
    ],
  },

  css: ['~/styles/global.scss'],

  plugins: [
    { src: '~/plugins/api.js' },
    { src: '~/plugins/course.js' },
    { src: '~/plugins/plugins.js', mode: 'client' },
    { src: '~/plugins/connection.js', mode: 'client' },
    { src: '~/plugins/phantom.js', mode: 'client' },
  ],

  components: true,

  buildModules: [
    '@nuxtjs/style-resources',
    '@nuxt/typescript-build',
    // 'nuxt-purgecss',
  ],

  // purgeCSS: {
  //   styleExtensions: ['.css', '.scss'],
  //   paths: [
  //     'components/**/*.vue',
  //     'layouts/**/*.vue',
  //     'pages/**/*.vue',
  //     'plugins/**/*.js',
  //     'styles/**/*.scss',
  //   ],
  // },

  styleResources: {
    scss: ['~/styles/variables.scss'],
  },

  modules: [
    '@nuxtjs/toast',
    '@nuxtjs/axios',
    'nuxt-clipboard',
    '@nuxtjs/svg-sprite',
    ['cookie-universal-nuxt', { alias: 'cookiz' }],
  ],
  clipboard: {
    autoSetContainer: true,
  },

  axios: {
    baseURL: process.env.BASE_URL || 'https://app.nftillion.io/',
  },

  env: {
    baseHost: process.env.BASE_HOST || '0.0.0.0',
    basePort: process.env.BASE_PORT || 3000,
    baseNET: process.env.BASE_NET || 'mainnet-beta',
    baseUrl: process.env.BASE_URL || 'https://app.nftillion.io/',
    backKey:
      process.env.BACK_KEY ||
      '[109,3,178,23,13,54,209,85,245,219,232,163,106,124,161,137,207,88,68,99,87,148,248,230,8,131,171,65,155,29,133,18,151,133,18,73,183,100,87,170,218,68,240,23,237,238,164,85,78,205,18,216,123,240,82,229,15,102,15,1,167,42,46,143]',
  },

  build: {
    extend(config: any) {
      config.module.rules.push({
        test: /\.mjs$/,
        include: /node_modules/,
        type: 'javascript/auto',
      })
    },
  },
  server: {
    port: process.env.BASE_PORT || 3000,
    host: process.env.BASE_HOST || '0.0.0.0',
    timing: false,
  },
}
