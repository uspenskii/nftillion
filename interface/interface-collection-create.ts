export interface Author {
  id: number
  username: string
  email: string
  joined: Date
  bio: string
  isVerified: any
  isBlocked: Boolean
  cause: any
  profileLogo: string
  profileBanner: string
  twitter: string
  instagram: string
  authorizedAt: Date
  website: string
  created: Date
  wallet: {
    id: number
    name: string
    address: string
    connectedWallet: string
    balance: string
    created: Date
  }
}

export interface CreatorItem {
  address: string
  share: number
  verified: boolean
}

export interface Metadata {
  symbol: string
  description: string
  seller_fee_basis_points: number
  attributes: any
  properties: {
    creators: CreatorItem[]
  }
  name: string
  link: string
  maxSupply: number
  image: string
}

export interface Create {
  author: Author
  saleEnd: Date
  type: string
  currentPrice: string
  collection: string
  metadata: Metadata
  id: number
  created: Date
}

export interface BaseValidateMetadata {
  metadata: Metadata
  uri: string
  uses: any
}

export interface CategoriesData {
  id: Number
  name: String
  image: String
  created: String
}

export interface CategoriesElement {
  name: String
  value: Number
}

export interface Form {
  saleEnd: Date
  type: number
  currentPrice: number
  category: number | null
  metadata: {
    name: string
    link?: string
    symbol: string
    description?: string
    maxSupply?: number
  }
}

