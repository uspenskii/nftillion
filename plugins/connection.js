import { clusterApiUrl } from '@solana/web3.js'
import { Connection } from '@metaplex/js'

export default async ({}, inject) => {
  inject('connection', new Connection(clusterApiUrl(process.env.baseNET)))
}
