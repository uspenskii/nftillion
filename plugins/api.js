import hasher from '../helpers/getToken.js'

export default function ({ $axios, store, $cookiz }, inject) {
  $axios.onRequest(() => {
    if ($cookiz.get('publicKey')) {
      let token = hasher($cookiz.get('publicKey'))
      $axios.setToken(token, 'Bearer')
    }
  })

  const api = $axios.create()

  api.setBaseURL('https://app.nftillion.io/')

  if ($cookiz.get('publicKey')) {
    store.dispatch('auth', $cookiz.get('publicKey'))
  }

  inject('api', api)
}
