async function phantom() {
  if (!window.hasOwnProperty('solana')) {
    return null
  }
  while (!window.hasOwnProperty('solana')) {
    await new Promise((resolve) => setTimeout(resolve, 1000))
  }

  const { solana } = window

  if (solana?.isPhantom) {
    return solana
  }
}

export default async ({ $cookiz, store }, inject) => {
  const wallet = await phantom()

  const cookiePublicKey = $cookiz.get('publicKey')

  const removeWallet = () => {
    $cookiz.remove('publicKey')
    $cookiz.remove('user_id')
    store.commit('logout')
  }

  if (cookiePublicKey) {
    try {
      await wallet.connect()
    } catch (error) {
      removeWallet()
    }
  } else {
    removeWallet()
  }

  if (wallet) {
    wallet.on('accountChanged', () => {
      removeWallet()
    })
  }

  if (wallet) {
    inject('phantom', wallet)
  }
}
