export default async ({ store, $axios }) => {
  $axios
    .$get(
      'https://api.coingecko.com/api/v3/simple/price?ids=solana&vs_currencies=usd'
    )
    .then((res) => {
      store.dispatch('setCourse', res.solana.usd)
    })
}
